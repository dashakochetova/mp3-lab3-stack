#include "tdatstack.h"
#include "gtest.h"

TEST(TStack, fill_up_to_a_maximum)
{
	const int size = 2;
	TStack A (size);
	A.Put(1);
	A.Put(1);
	EXPECT_EQ(size, A.GetCount());
}

TEST(TStack, fill_stack_compared_with_the_size)
{
	const int size = 2;
	TStack A(size);
	A.Put(1);
	EXPECT_NE(size, A.GetCount());
}

TEST(TStack, fill_up_to_a_maximum_and_removal_to_zero)
{
	const int size = 2;
	TStack A(size);
	A.Put(1);
	A.Put(1);
	A.Get();
	A.Get();
	EXPECT_EQ(0, A.GetCount());
}

TEST(TStack, fill_stack_and_removal_compared_with_the_zero)
{
	const int size = 2;
	TStack A(size);
	A.Put(1);
	A.Put(1);
	A.Get();
	EXPECT_NE(0, A.GetCount());
}


TEST(TStack, memory_is_empty_when_adding)
{
	TStack A(0);
	A.Put(1);
	EXPECT_EQ(DataNoMem, A.GetRetCode());
}

TEST(TStack, memory_is_empty_when_removing)
{
	TStack A(0);
	A.Get();
	EXPECT_EQ(DataNoMem, A.GetRetCode());
}

TEST(TStack, all_is_well)
{
	const int size = 2;
	TStack A(size);
	A.Put(1);
	A.Put(1);
	EXPECT_EQ(DataOK, A.GetRetCode() );
}

TEST(TStack, well_gets_value)
{
	const int size = 2;
	TStack A(size);
	A.Put(1);
	A.Put(2);
	EXPECT_EQ(2, A.Get() );
	EXPECT_EQ(1, A.Get() );
}

TEST(TStack, can_get_code_return_is_full)
{
	const int size = 2;
	TStack A(size);
	A.Put(1);
	A.Put(1);
	A.Put(1);
	EXPECT_EQ(DataFull, A.GetRetCode() );
}

TEST(TStack, can_get_code_return_is_empty)
{
	const int size = 2;
	TStack A(size);
	A.Get();
	EXPECT_EQ(DataEmpty, A.GetRetCode() );
}



